package centralProcessor;

public class CP {
    public static void main(String[] args) {
        AddressBufferRegister addressBufferRegister = new AddressBufferRegister();
        ArithmeticAndLogicDevice arithmeticAndLogicDevice = new ArithmeticAndLogicDevice();
        BlockOfRegisteredMemory blockOfRegisteredMemory = new BlockOfRegisteredMemory();
        BufferDataRegister bufferDataRegister = new BufferDataRegister();
        ControlDevice controlDevice = new ControlDevice();
        ControlLine controlLine = new ControlLine();
        DataBackbone dataBackbone = new DataBackbone();
        StreetAddress streetAddress = new StreetAddress();
        TheInternalDataHighway theInternalDataHighway = new TheInternalDataHighway();
    }

}
