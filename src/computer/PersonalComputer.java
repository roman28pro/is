package computer;

public class PersonalComputer {
    public static void main(String[] args) {

        AlgorithmForCheckingSignalsForCorrectness algorithmForCheckingSignalsForCorrectness = new AlgorithmForCheckingSignalsForCorrectness();
        Alphabet alphabet = new Alphabet();
        CommandEncryptionKey commandEncryptionKey = new CommandEncryptionKey();
        DataProcessor dataProcessor = new DataProcessor();
        PersonalComputer personalComputer = new PersonalComputer();
        Receiver receiver = new Receiver();
        Signal signal = new Signal();
        SignalEncryptionKey signalEncryptionKey = new SignalEncryptionKey();
        SignalHandler signalHandler = new SignalHandler();
        SignalProcessingAlgorithm signalProcessingAlgorithm = new SignalProcessingAlgorithm();
        SignalProcessingTable signalProcessingTable = new SignalProcessingTable();
        SignalTable signalTable = new SignalTable();
        Symbols symbols = new Symbols();
        Transmitter transmitter = new Transmitter();
    }
}
